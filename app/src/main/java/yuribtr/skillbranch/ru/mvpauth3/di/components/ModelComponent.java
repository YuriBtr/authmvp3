package yuribtr.skillbranch.ru.mvpauth3.di.components;

import javax.inject.Singleton;

import dagger.Component;
import yuribtr.skillbranch.ru.mvpauth3.di.modules.ModelModule;
import yuribtr.skillbranch.ru.mvpauth3.mvp.models.AbstractModel;

@Component(modules = ModelModule.class)
@Singleton
public interface ModelComponent {
    void inject(AbstractModel abstractModel);
}
