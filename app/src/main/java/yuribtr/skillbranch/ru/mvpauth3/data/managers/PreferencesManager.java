package yuribtr.skillbranch.ru.mvpauth3.data.managers;

import android.content.Context;
import android.content.SharedPreferences;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

public class PreferencesManager{
    private final SharedPreferences mSharedPreferences;

    public PreferencesManager(Context context) {
       mSharedPreferences = getDefaultSharedPreferences(context);
    }

    public void saveAuthToken (String authToken){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.AUTH_TOKEN, authToken);
        editor.apply();
    }

    public String getAuthToken (){
        return mSharedPreferences.getString(ConstantManager.AUTH_TOKEN, null);
    }

}
