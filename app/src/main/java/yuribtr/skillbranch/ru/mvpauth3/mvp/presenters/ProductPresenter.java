package yuribtr.skillbranch.ru.mvpauth3.mvp.presenters;

import javax.inject.Inject;

import dagger.Provides;
import yuribtr.skillbranch.ru.mvpauth3.data.storage.dto.ProductDto;
import yuribtr.skillbranch.ru.mvpauth3.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth3.di.scopes.ProductScope;
import yuribtr.skillbranch.ru.mvpauth3.mvp.models.ProductModel;
import yuribtr.skillbranch.ru.mvpauth3.mvp.views.IProductView;

public class ProductPresenter extends AbstractPresenter<IProductView> implements IProductPresenter{
    private static final String TAG = "ProductPresenter";

    @Inject
    ProductModel mProductModel;
    private ProductDto mProduct;

    public ProductPresenter(ProductDto product) {
        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);
        mProduct = product;
    }


    @Override
    public void initView() {
        if (getView() != null) {
            getView().showProductView(mProduct);
        }
    }

    @Override
    public void clickOnPlus() {
        mProduct.addProduct();
        mProductModel.updateProduct(mProduct);
        if (getView() != null) {
            getView().updateProductCountView(mProduct);
        }
    }

    @Override
    public void clickOnMinus() {
        if (mProduct.getCount() > 1) {
            mProduct.deleteProduct();
            mProductModel.updateProduct(mProduct);
            if (getView() != null) {
                getView().updateProductCountView(mProduct);
            }
        }
    }

    //region================DI================
    private Component createDaggerComponent(){
        return DaggerProductPresenter_Component.builder()
                .module(new Module())
                .build();
    }

    @dagger.Module
    public class Module {
        @Provides
        @ProductScope
        ProductModel provideProductModel() {
            return new ProductModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @ProductScope
    interface Component {
        void inject(ProductPresenter presenter);
    }
    //endregion
}
