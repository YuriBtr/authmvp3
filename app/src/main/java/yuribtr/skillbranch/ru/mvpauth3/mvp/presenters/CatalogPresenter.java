package yuribtr.skillbranch.ru.mvpauth3.mvp.presenters;

import java.util.List;

import javax.inject.Inject;

import dagger.Provides;
import yuribtr.skillbranch.ru.mvpauth3.R;
import yuribtr.skillbranch.ru.mvpauth3.data.storage.dto.ProductDto;
import yuribtr.skillbranch.ru.mvpauth3.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth3.di.scopes.CatalogScope;
import yuribtr.skillbranch.ru.mvpauth3.mvp.models.CatalogModel;
import yuribtr.skillbranch.ru.mvpauth3.mvp.views.ICatalogView;
import yuribtr.skillbranch.ru.mvpauth3.mvp.views.IRootView;
import yuribtr.skillbranch.ru.mvpauth3.ui.activities.RootActivity;

public class CatalogPresenter extends AbstractPresenter<ICatalogView> implements ICatalogPresenter{

    @Inject
    RootPresenter mRootPresenter;

    @Inject
    CatalogModel mCatalogModel;
    private List<ProductDto> mProductDtoList;

    public CatalogPresenter() {
        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);
    }

    @Override
    public void initView() {
        if (mProductDtoList == null) {
            mProductDtoList = mCatalogModel.getProductList();
        }
        if (getView() != null){
            getView().showCatalogView(mProductDtoList);
        }
    }

    @Override
    public void clickOnBuyButton(int position) {
        if (getView() != null) {
            if (checkUserAuth()) {
                //todo:getString(R.string.goods)+mProductDtoList.get(position).getProductName()+getString(R.string.successfully_added_to_cart
                getRootView().showMessage("Товар "+mProductDtoList.get(position).getProductName()+" успешно добавлен в корзину");
                //getView().showAddToCartMessage(mProductDtoList.get(position));
                getView().updateProductCounter();
            } else {
                getView().showAuthScreen();
            }
        }
    }

    private IRootView getRootView() {
        return mRootPresenter.getView();
    }

    @Override
    public boolean checkUserAuth() {
        return mCatalogModel.isUserAuth();
    }

    //todo: ДЗ 01:32:42
    //region================DI================
    private Component createDaggerComponent() {
        return DaggerCatalogPresenter_Component.builder()
                .component(DaggerService.getComponent(RootActivity.Component.class))
                .module(new Module())
                .build();
    }

    @dagger.Module
    public class Module {
        @Provides
        @CatalogScope
        CatalogModel provideCatalogModel() {
            return new CatalogModel();
        }
    }

    //CatalogPresenter наследуется от RootActivity.Component
    //RootActivity.Component инжектит  RootPresenter в RootActivity
    //чтобы из CatalogPresenter получить RootPresenter указываем в CatalogPresenter.Component что он наследован от RootActivity.Component
    @dagger.Component(dependencies = RootActivity.Component.class, modules = Module.class)
    @CatalogScope
    interface Component {
        void inject (CatalogPresenter presenter);
    }
    //endregion
}
