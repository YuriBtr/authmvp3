package yuribtr.skillbranch.ru.mvpauth3.ui.fragments;


import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.ActivityChooserView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Component;
import dagger.Provides;
import yuribtr.skillbranch.ru.mvpauth3.R;
import yuribtr.skillbranch.ru.mvpauth3.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth3.di.components.DaggerPicassoComponent;
import yuribtr.skillbranch.ru.mvpauth3.di.components.PicassoComponent;
import yuribtr.skillbranch.ru.mvpauth3.di.modules.PicassoCacheModule;
import yuribtr.skillbranch.ru.mvpauth3.di.scopes.CatalogScope;
import yuribtr.skillbranch.ru.mvpauth3.mvp.presenters.CatalogPresenter;
import yuribtr.skillbranch.ru.mvpauth3.ui.activities.RootActivity;
import yuribtr.skillbranch.ru.mvpauth3.utils.CircleTransform;


public class AccountFragment extends Fragment implements View.OnClickListener {

    Target mTarget;

    @BindView(R.id.add_address_btn)
    Button mAddAddressBtn;

    @BindView(R.id.fab)
    FloatingActionButton mFab;

    @Inject
    Picasso mPicasso;

    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        ButterKnife.bind(this, view);

        PicassoComponent component = DaggerService.getComponent(PicassoComponent.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(PicassoComponent.class, component);
        }
        component.inject(this);

        mAddAddressBtn.setOnClickListener(this);
        getActivity().setTitle(getString(R.string.personal_cabinet));

        mTarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                mFab.setImageDrawable(new BitmapDrawable(getContext().getResources(), bitmap));
            }
            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }
            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        };
        mPicasso.load(R.drawable.avatar).resize(550,450).transform(new CircleTransform()).into(mTarget);

        return view;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_address_btn) {
            RootActivity rootActivity = (RootActivity) getActivity();
            rootActivity.showAddLocationFragment();
            //mPresenter.clickOnBuyButton(mProductPager.getCurrentItem());
        }
    }

    private PicassoComponent createDaggerComponent(){
        return DaggerPicassoComponent.builder()
                .picassoCacheModule(new PicassoCacheModule())
                .build();
    }


}
