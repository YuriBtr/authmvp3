package yuribtr.skillbranch.ru.mvpauth3;

import android.app.Application;

import yuribtr.skillbranch.ru.mvpauth3.di.components.AppComponent;
import yuribtr.skillbranch.ru.mvpauth3.di.components.DaggerAppComponent;
import yuribtr.skillbranch.ru.mvpauth3.di.modules.AppModule;
import yuribtr.skillbranch.ru.mvpauth3.utils.ComponentReflectionInjector;
import yuribtr.skillbranch.ru.mvpauth3.utils.Injector;

public class App extends Application implements Injector {
    private ComponentReflectionInjector<AppComponent> injector;

    private static AppComponent sAppComponent;

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        AppComponent sAppComponent = DaggerAppComponent.builder()
//                .appModule(new AppModule(this))
//                .build();
//        injector = new ComponentReflectionInjector<>(AppComponent.class, sAppComponent);

        createComponent();
    }

    private void createComponent() {
        sAppComponent = DaggerAppComponent.builder()//todo: w/o new AppModule(
                .appModule(new AppModule(getApplicationContext()))
                .build();
    }

    @Override
    public void inject(Object target) {
        injector.inject(target);
    }
}
