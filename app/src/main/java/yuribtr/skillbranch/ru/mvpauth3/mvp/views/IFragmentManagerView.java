package yuribtr.skillbranch.ru.mvpauth3.mvp.views;

public interface IFragmentManagerView extends IRootView{
    void goToPrevFragment();
}

