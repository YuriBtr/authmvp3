package yuribtr.skillbranch.ru.mvpauth3.mvp.presenters;

import android.util.Log;

import javax.inject.Inject;

import dagger.Provides;
import yuribtr.skillbranch.ru.mvpauth3.R;
import yuribtr.skillbranch.ru.mvpauth3.data.managers.DataManager;

import yuribtr.skillbranch.ru.mvpauth3.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth3.di.scopes.AuthScope;
import yuribtr.skillbranch.ru.mvpauth3.mvp.models.AuthModel;
import yuribtr.skillbranch.ru.mvpauth3.mvp.models.IAuthModel;
import yuribtr.skillbranch.ru.mvpauth3.mvp.views.IAuthView;
import yuribtr.skillbranch.ru.mvpauth3.ui.custom_views.AuthPanel;
import yuribtr.skillbranch.ru.mvpauth3.data.managers.ConstantManager;

public class AuthPresenter extends AbstractPresenter<IAuthView> implements IAuthPresenter, IAuthNotifier{
    private static final String TAG = "AuthPresenter";

    @Inject
    IAuthModel mAuthModel;

    private boolean mIsCatalogLoading=false;

    public AuthPresenter() {
        mAuthModel = new AuthModel(this);
        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);

        Log.e(TAG, "AuthPresenter: inject completed");
    }


    @Override
    public void initView() {
        if (getView()!= null) {
            if (checkUserAuth()) {
                getView().hideLoginBtn();
            } else {
                getView().showLoginBtn();
            }
            if (mIsCatalogLoading) getView().showLoad();
            else getView().hideLoad();
        }
    }

    @Override
    public void clickOnLogin() {
        if (getView()!= null && getView().getAuthPanel()!= null) {
            if (getView().getAuthPanel().isIdle()) {
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
            } else {
                boolean emailOk = getView().getEmail().matches(ConstantManager.PATTERN_EMAIL);
                boolean passwordOk = getView().getPassword().matches(ConstantManager.PATTERN_PASSWORD);
                if (emailOk && passwordOk) {
                    getView().showMessage(getView().getContext().getString(R.string.user_authenticating_message));
                    getView().showLoginProgress();
                    mAuthModel.loginUser(getView().getUserEmail(), getView().getUserPassword());
                } else {
                    if (!emailOk) getView().setWrongEmailError();
                    if (!passwordOk) getView().setWrongPasswordError();
                    getView().showMessage(getView().getContext().getString(R.string.email_or_password_wrong_format));
                }
            }
        }
    }

    @Override
    public void clickOnFb() {
        if (getView()!= null) {
            getView().showMessage("clickOnFb");
        }
    }

    @Override
    public void clickOnVk() {
        if (getView()!= null) {
            getView().showMessage("clickOnVk");
        }
    }

    @Override
    public void clickOnTwitter() {
        if (getView()!= null) {
            getView().showMessage("clickOnTwitter");
        }
    }

    @Override
    public void clickOnShowCatalog() {
        mIsCatalogLoading = true;
        if (getView()!= null) {
            //getView().showMessage(getView().getContext().getString(R.string.catalog_loading_message));
            getView().showCatalogScreen();
            mIsCatalogLoading = false;
        }
    }

    @Override
    public boolean checkUserAuth() {
        return mAuthModel.isAuthUser();
    }

    @Override
    public void onPasswordChanged() {
        if (getView()!= null) {
            if (getView().getPassword().matches(ConstantManager.PATTERN_PASSWORD))
                getView().setAcceptablePassword();
            else
                getView().setNonAcceptablePassword();

        }
    }

    @Override
    public void onEmailChanged() {
        if (getView()!= null) {
            if (getView().getEmail().matches(ConstantManager.PATTERN_EMAIL))
                getView().setAcceptableEmail();
            else
                getView().setNonAcceptableEmail();
        }
    }

    @Override
    public void onLoginSuccess() {
        if (getView()!= null && getView().getAuthPanel()!= null) {
            getView().showMessage(getView().getContext().getString(R.string.authentificate_successful));
            getView().hideLoginProgress();
            getView().hideLoginBtn();
            getView().getAuthPanel().setCustomState(AuthPanel.IDLE_STATE);
            getView().showCatalogScreen();
        }
    }

    @Override
    public void onLoginError(String message) {
        if (getView()!= null) {
            getView().hideLoginProgress();
            getView().showMessage(message);
        }
    }

    //region================DI================
    @dagger.Module
    public class Module {
        @Provides
        @AuthScope
        IAuthModel provideAuthModel(IAuthNotifier presenter){
            return new AuthModel(presenter);
        }
    }

    @dagger.Component(modules = Module.class)
    @AuthScope
    interface Component {
        void inject(IAuthNotifier presenter);
    }


    //делаем ребилд, чтобы появился DaggerAuthPresenterComponent и authPresenterModule
    private Component createDaggerComponent(){
        return DaggerAuthPresenter_Component.builder()
                .module(new Module())
                .build();
    }
    //endregion


}
