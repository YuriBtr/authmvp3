package yuribtr.skillbranch.ru.mvpauth3.mvp.presenters;

public interface IAuthNotifier {
    void onLoginSuccess();
    void onLoginError(String message);
}
