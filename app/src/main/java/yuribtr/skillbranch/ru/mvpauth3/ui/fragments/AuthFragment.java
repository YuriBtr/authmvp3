package yuribtr.skillbranch.ru.mvpauth3.ui.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;
import yuribtr.skillbranch.ru.mvpauth3.R;
import yuribtr.skillbranch.ru.mvpauth3.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth3.di.scopes.AuthScope;

import yuribtr.skillbranch.ru.mvpauth3.mvp.presenters.AuthPresenter;
import yuribtr.skillbranch.ru.mvpauth3.mvp.presenters.DaggerAuthPresenter_Component;
import yuribtr.skillbranch.ru.mvpauth3.mvp.presenters.IAuthPresenter;
import yuribtr.skillbranch.ru.mvpauth3.mvp.views.IAuthView;
import yuribtr.skillbranch.ru.mvpauth3.mvp.views.IFragmentManagerView;
import yuribtr.skillbranch.ru.mvpauth3.ui.custom_views.AuthPanel;

public class AuthFragment extends Fragment implements IAuthView, View.OnClickListener{
    private static final String TAG = "AuthFragment";

    @Inject
    AuthPresenter mPresenter;

    private int mEmailTextColor= Color.BLACK;
    private int mPasswordTextColor=Color.BLACK;

    @BindView(R.id.auth_card)
    CardView mAuthCard;

    @BindView(R.id.auth_wrapper)
    AuthPanel mAuthPanel;

    @BindView(R.id.login_email_et)
    EditText mEmailText;

    @BindView(R.id.login_password_et)
    EditText mPasswordText;

    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;

    @BindView(R.id.login_btn)
    Button mLoginBtn;

    @BindView(R.id.login_email_wrap)
    TextInputLayout mEmailWrap;

    @BindView(R.id.login_password_wrap)
    TextInputLayout mPasswordWrap;

    @BindView(R.id.enter_pb)
    ProgressBar mEnterProgressBar;


    //region================LifeCycle================
    public AuthFragment() {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        if (mAuthPanel.getCustomState() == AuthPanel.IDLE_STATE) {
                            getParentActivity().goToPrevFragment();
                            return true;
                        }
                        else {
                            mAuthPanel.setCustomState(AuthPanel.IDLE_STATE);
                            return true;
                        }
                    }
                }
                return false;
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_auth, container, false);
        ButterKnife.bind(this, view);
        getActivity().setTitle(getString(R.string.login));
        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);

        mPresenter.takeView(this);
        mPresenter.initView();
        mLoginBtn.setOnClickListener(this);
        mShowCatalogBtn.setOnClickListener(this);
        mEmailTextColor = mEmailText.getCurrentTextColor();
        mPasswordTextColor = mPasswordText.getCurrentTextColor();


        mPasswordText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //todo: inject mPresenter
                mPresenter.onPasswordChanged();
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mEmailText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //todo: inject mPresenter
                mPresenter.onEmailChanged();
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        return view;
    }

    public String getUserEmail(){//#01:21
        return  String.valueOf(mEmailText.getText());
    }

    public String getUserPassword(){
        return  String.valueOf(mPasswordText.getText());
    }

    @Override
    public void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }
    //endregion

    //region================IAuthView================
    @Override
    public IAuthPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mLoginBtn.setVisibility(View.GONE);
    }

    @Override
    public String getEmail() {
        return mEmailText.getText().toString();
    }

    @Override
    public String getPassword() {
        return mPasswordText.getText().toString();
    }

    @Override
    public void setNonAcceptableEmail() {
        mEmailText.setTextColor(Color.RED);
        //to workaround rotating
        if (!mPasswordText.getText().toString().isEmpty() || !mEmailText.getText().toString().isEmpty())
            mLoginBtn.setEnabled(false);
    }

    @Override
    public void setNonAcceptablePassword() {
        mPasswordText.setTextColor(Color.RED);
        //to workaround rotating
        if (!mPasswordText.getText().toString().isEmpty() || !mEmailText.getText().toString().isEmpty())
            mLoginBtn.setEnabled(false);
    }

    @Override
    public void setAcceptableEmail() {
        mEmailWrap.setErrorEnabled(false);
        mEmailText.setTextColor(mEmailTextColor);
        if (!mPasswordWrap.isErrorEnabled() && mPasswordText.getText()!=null && mPasswordText.getText().length()>0) mLoginBtn.setEnabled(true);
    }

    @Override
    public void setAcceptablePassword() {
        mPasswordWrap.setErrorEnabled(false);
        mPasswordText.setTextColor(mPasswordTextColor);
        if (!mEmailWrap.isErrorEnabled() && mEmailText.getText()!=null && mEmailText.getText().length()>0) mLoginBtn.setEnabled(true);
    }

    @Override
    public void setWrongEmailError() {
        mEmailWrap.setErrorEnabled(true);
        mEmailWrap.setError(getString(R.string.email_input_error));
        mLoginBtn.setEnabled(false);
    }

    @Override
    public void setWrongPasswordError() {
        mPasswordWrap.setErrorEnabled(true);
        mPasswordWrap.setError(getString(R.string.password_input_error));
        mLoginBtn.setEnabled(false);
    }

    @Override
    public void removeWrongEmailError() {
        mEmailWrap.setErrorEnabled(false);
    }

    @Override
    public void removeWrongPasswordError() {
        mPasswordWrap.setErrorEnabled(false);
    }

    @Override
    public Context getContext() {
        return getActivity().getApplicationContext();
    }

    @Override
    public void showLoginProgress() {
        mLoginBtn.setVisibility(View.GONE);
        mEnterProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginProgress() {
        mLoginBtn.setVisibility(View.VISIBLE);
        mEnterProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showCatalogScreen() {
        getParentActivity().goToPrevFragment();
    }

    @Nullable
    @Override
    public AuthPanel getAuthPanel() {
        return mAuthPanel;
    }
    //endregion

    //region================IView================
    @Override
    public void showMessage(String message) {
        getParentActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable e) {
        getParentActivity().showError(e);
    }

    @Override
    public void showLoad() {
        getParentActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getParentActivity().hideLoad();
    }
    //endregion

    //region================ClickListener================
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.show_catalog_btn:
                mPresenter.clickOnShowCatalog();
                break;
            case R.id.login_btn:
                mPresenter.clickOnLogin();
                break;
        }
    }
    //endregion

    private IFragmentManagerView getParentActivity() {
        return (IFragmentManagerView) getActivity();
    }

    //region================DI================
    private Component createDaggerComponent() {
        return DaggerAuthFragment_Component.builder()
                .module(new Module())
                .build();
    }

    @dagger.Module
    public class Module {
        @Provides
        @AuthScope
        AuthPresenter provideAuthPresenter() {
            return new AuthPresenter();
        }
    }

    @dagger.Component(modules = Module.class)
    @AuthScope
    interface Component {
        void inject(AuthFragment fragment);
    }
    //endregion
}
