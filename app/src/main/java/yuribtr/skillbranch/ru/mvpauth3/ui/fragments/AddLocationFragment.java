package yuribtr.skillbranch.ru.mvpauth3.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import yuribtr.skillbranch.ru.mvpauth3.R;


public class AddLocationFragment extends Fragment {


    public AddLocationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(getString(R.string.add_delivery_address));
        return inflater.inflate(R.layout.fragment_add_location, container, false);
    }

}
