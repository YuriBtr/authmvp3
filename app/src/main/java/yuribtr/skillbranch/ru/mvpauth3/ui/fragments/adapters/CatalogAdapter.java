package yuribtr.skillbranch.ru.mvpauth3.ui.fragments.adapters;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import yuribtr.skillbranch.ru.mvpauth3.data.storage.dto.ProductDto;
import yuribtr.skillbranch.ru.mvpauth3.ui.fragments.ProductFragment;

public class CatalogAdapter extends FragmentStatePagerAdapter  {
    private List<ProductDto> mProductList = new ArrayList<>();

    public CatalogAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return ProductFragment.newInstance(mProductList.get(position));
    }

    @Override
    public int getCount() {
        return mProductList.size();
    }

    public void addItem(ProductDto product) {
        mProductList.add(product);
        notifyDataSetChanged();
    }
}
