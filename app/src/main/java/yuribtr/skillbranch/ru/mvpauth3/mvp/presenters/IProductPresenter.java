package yuribtr.skillbranch.ru.mvpauth3.mvp.presenters;

public interface IProductPresenter {
    void clickOnPlus();
    void clickOnMinus();
}
