package yuribtr.skillbranch.ru.mvpauth3.di.components;

import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;
import yuribtr.skillbranch.ru.mvpauth3.di.modules.PicassoCacheModule;
import yuribtr.skillbranch.ru.mvpauth3.ui.fragments.AccountFragment;
import yuribtr.skillbranch.ru.mvpauth3.ui.fragments.ProductFragment;

@Component(dependencies = AppComponent.class, modules = PicassoCacheModule.class)
@Singleton
public interface PicassoComponent {
    Picasso getPicasso();
    void inject(AccountFragment fragment);
}
