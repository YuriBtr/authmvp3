package yuribtr.skillbranch.ru.mvpauth3.ui.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;
import yuribtr.skillbranch.ru.mvpauth3.BuildConfig;
import yuribtr.skillbranch.ru.mvpauth3.R;
import yuribtr.skillbranch.ru.mvpauth3.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth3.di.scopes.RootScope;
import yuribtr.skillbranch.ru.mvpauth3.mvp.presenters.RootPresenter;
import yuribtr.skillbranch.ru.mvpauth3.mvp.views.IFragmentManagerView;
import yuribtr.skillbranch.ru.mvpauth3.ui.fragments.AccountFragment;
import yuribtr.skillbranch.ru.mvpauth3.ui.fragments.AddLocationFragment;
import yuribtr.skillbranch.ru.mvpauth3.ui.fragments.AuthFragment;
import yuribtr.skillbranch.ru.mvpauth3.ui.fragments.CatalogFragment;
import yuribtr.skillbranch.ru.mvpauth3.data.managers.ConstantManager;
import yuribtr.skillbranch.ru.mvpauth3.utils.TransformRoundedImage;

public class RootActivity extends BaseActivity implements IFragmentManagerView, NavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener{
    //так как у этой активити пока нет презентера, хранить счетчик негде, поэтому и статическая переменная (позже будет переделано, с появлением презентера)
    private static int sGoodsCounter = 0;
    Menu mMenu;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawer;
    @BindView(R.id.nav_view)
    NavigationView mNavigationView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorContainer;
    @BindView(R.id.fragment_container)
    FrameLayout mFragmentContainer;

    ImageView mUserAvatar;
    ImageView mCartBadgeIcon;
    TextView mCartBadgeText;

    FragmentManager mFragmentManager;

    @Inject
    RootPresenter mRootPresenter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        ButterKnife.bind(this);

        //предпоследний шаг, делается перед генерацией DaggerRootActivity_Component
        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);

        initToolbar();
        initDrawer();
        mRootPresenter.takeView(this);
        mRootPresenter.initView();

        //todo: init view
        mUserAvatar = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.user_avatar);
        //todo: fix this
        mFragmentManager = getSupportFragmentManager();

//        mFragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
//            @Override
//            public void onBackStackChanged() {
//                showMessage("changed");
//            }
//        });

        if (savedInstanceState == null)
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, new CatalogFragment())
                    .commit();
    }

    @Override
    protected void onDestroy() {
        mRootPresenter.dropView();
        super.onDestroy();
    }



    private void initDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.open_drawer, R.string.close_drawer);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);
    }


    private void initToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null) {
//            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
    //todo:show icon "back" at action bar


    public void setCartQuantity(int quantity){
        sGoodsCounter = sGoodsCounter + quantity;
    }

    public void updateCartQuantity(){
        if (sGoodsCounter>0) {
            mCartBadgeIcon.setImageResource(R.drawable.ic_shopping_cart_black_24dp);
            mCartBadgeText.setText(String.valueOf(sGoodsCounter));
            mCartBadgeText.setVisibility(View.VISIBLE);
        } else {
            mCartBadgeIcon.setImageResource(R.drawable.ic_shopping_cart_grey_24dp);
            mCartBadgeText.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu = menu;
        getMenuInflater().inflate(R.menu.badge_menu, mMenu);
        MenuItem item = mMenu.findItem(R.id.badge);
        MenuItemCompat.setActionView(item, R.layout.cart_badge);
        LinearLayout relativeLayout = (LinearLayout) MenuItemCompat.getActionView(item);
        mCartBadgeIcon = (ImageView) relativeLayout.findViewById(R.id.cart_icon_iv);
        mCartBadgeText = (TextView) relativeLayout.findViewById(R.id.cart_count_tv);
        setCartQuantity(0);
        updateCartQuantity();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START))
            mDrawer.closeDrawer(GravityCompat.START);
        else if (mFragmentManager.getBackStackEntryCount() > 0) {mFragmentManager.popBackStack();}
        else showDialog(ConstantManager.CHECK_EXIT);
        //moveTaskToBack(true);
    }


    protected Dialog onCreateDialog(int id) {
        if (id == ConstantManager.CHECK_EXIT) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.check_exit_title);
            builder.setMessage(R.string.check_exit_question);
            builder.setIcon(android.R.drawable.ic_dialog_info);
            builder.setPositiveButton(R.string.yes, myClickListener);
            builder.setNegativeButton(R.string.no, myClickListener);
            return builder.create();
        }
        return super.onCreateDialog(id);
    }

    DialogInterface.OnClickListener myClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    moveTaskToBack(true);
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.nav_account:
                fragment = new AccountFragment();
                item.setChecked(true);
                break;
            case R.id.nav_catalog:
                fragment = new CatalogFragment();
                item.setChecked(true);
                break;
            case R.id.nav_favorite:
                break;
            case R.id.nav_orders:
                break;
            case R.id.nav_notification:
                break;
        }
        if (fragment != null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null)
                    .commit();
        }
        mDrawer.closeDrawer(GravityCompat.START);
        return false;
    }

    public void showAuthFragment(){
        Fragment fragment = new AuthFragment();
        if (fragment != null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    public void showAddLocationFragment(){
        Fragment fragment = new AddLocationFragment();
        if (fragment != null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    public void goToPrevFragment(){
        if (mFragmentManager.getBackStackEntryCount() > 0) {
            mFragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    //region================IRootView================
    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.unknown_error));
            //todo:send error stacktrace to crashlytics
        }

    }

    @Override
    public void showLoad() {
        showProgress();
    }

    @Override
    public void hideLoad() {
        hideProgress();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        showMessage("Moved to position: "+position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
    //endregion

    //region================DI================

    //сначала создается модуль
    @dagger.Module
    public class Module {
        @Provides
        @RootScope
        RootPresenter provideRootPresenter() {
            return new RootPresenter();
        }
    }

    //затем создается компонент
    @dagger.Component(modules = Module.class) //берем RootPresenter и инжектим в RootActivity
    @RootScope    //RootActivity.Component инжектит  RootPresenter в RootActivity
    public interface Component {
        void inject(RootActivity activity);

        RootPresenter getRootPresenter();//явно указываем что имеем доступ к RootPresenter из дочерних компонентов
        //чтобы из CatalogPresenter получить RootPresenter указываем в CatalogPresenter.Component что он наследован от RootActivity.Component
    }

    private Component createDaggerComponent(){
        //DaggerRootActivity_Component генерится автоматически в последнюю очередь
        return DaggerRootActivity_Component.builder()
                .module(new Module())
                .build();
    }
    //endregion

}