package yuribtr.skillbranch.ru.mvpauth3.di.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import yuribtr.skillbranch.ru.mvpauth3.data.managers.PreferencesManager;

@Module
public class LocalModule {

    @Provides
    @Singleton
    PreferencesManager providePreferencesManager(Context context){
        return new PreferencesManager(context);
    }
}
