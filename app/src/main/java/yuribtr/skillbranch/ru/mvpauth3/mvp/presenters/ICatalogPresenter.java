package yuribtr.skillbranch.ru.mvpauth3.mvp.presenters;

public interface ICatalogPresenter {
    void clickOnBuyButton(int position);
    boolean checkUserAuth();
}
