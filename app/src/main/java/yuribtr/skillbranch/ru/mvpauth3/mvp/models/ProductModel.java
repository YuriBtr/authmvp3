package yuribtr.skillbranch.ru.mvpauth3.mvp.models;

import yuribtr.skillbranch.ru.mvpauth3.data.managers.DataManager;
import yuribtr.skillbranch.ru.mvpauth3.data.storage.dto.ProductDto;

public class ProductModel extends AbstractModel {

    public ProductDto getProductById(int productId){
        return mDataManager.getProductById(productId);
    }

    public void updateProduct(ProductDto product){
        mDataManager.updateProduct(product);
    }
}
