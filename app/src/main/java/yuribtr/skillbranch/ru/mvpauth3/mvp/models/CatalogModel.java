package yuribtr.skillbranch.ru.mvpauth3.mvp.models;

import java.util.List;

import yuribtr.skillbranch.ru.mvpauth3.data.managers.DataManager;
import yuribtr.skillbranch.ru.mvpauth3.data.storage.dto.ProductDto;

public class CatalogModel extends AbstractModel{

    public List<ProductDto> getProductList() {
        return mDataManager.getMockProductList();
    }

    public boolean isUserAuth() {
        return mDataManager.isAuthUser();
    }
}
